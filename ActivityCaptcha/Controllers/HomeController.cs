using ActivityCaptcha.Controllers;
using CMS.DocumentEngine;
using Kentico.Content.Web.Mvc;
using Kentico.Content.Web.Mvc.Routing;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterPageRoute("Home", typeof(HomeController))]

namespace ActivityCaptcha.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPageRetriever pageRetriever;
        private readonly IPageDataContextInitializer pageDataContextInitializer;

        // Gets instances of required services using dependency injection
        public HomeController(IPageRetriever pageRetriever,
            IPageDataContextInitializer pageDataContextInitializer)
        {
            this.pageRetriever = pageRetriever;
            this.pageDataContextInitializer = pageDataContextInitializer;
        }

        // GET: Home
        public ActionResult Index()
        {
            // Retrieves a page from the Xperience database with the '/Home' node alias path
            TreeNode page = pageRetriever.Retrieve<TreeNode>(query => query
                                .Path("/Home", PathTypeEnum.Single))
                                .FirstOrDefault();

            // Responds with the HTTP 404 error when the page is not found
            if (page == null)
            {
                return HttpNotFound();
            }

            // Initializes the page data context using the retrieved page
            pageDataContextInitializer.Initialize(page);

            return View();
        }
    }
}