using ActivityCaptcha.App_Start;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;

namespace ActivityCaptcha
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {

            // Gets the ApplicationBuilder instance
            // Allows you to enable and configure Xperience MVC features
            ApplicationBuilder builder = ApplicationBuilder.Current;

            // Enables the page builder feature
            builder.UsePageBuilder();

            // Enables and configures selected Kentico ASP.NET MVC integration features
            ApplicationConfig.RegisterFeatures(builder);

            // Registers routes including system routes for enabled features
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            // Registers enabled bundles
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Registers the dependency resolver for the application
            DependencyResolverConfig.Register();
        }
    }
}
