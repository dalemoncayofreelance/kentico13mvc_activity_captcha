﻿using CMS.DataEngine;
using CMS.SiteProvider;
using Kentico.Forms.Web.Mvc;

namespace ActivityCaptcha.FormBuilder.FormComponents.FormComponentProperties
{
    public class InvisibleReCaptchaV3Properties : FormComponentProperties<string>
    {
        public InvisibleReCaptchaV3Properties()
            : base(FieldDataType.LongText, size: 1000)
        {
            DefaultValue = "";
            GoogleSecretKey = _googleSecretKey;
        }

        static string _googleSiteKey = "";
        static string _googleSecretKey = GoogleSecretKey;

        public static string GoogleSiteKey
        {
            get
            {
                return SettingsKeyInfoProvider.GetValue("reCaptchaV3SiteKey", SiteContext.CurrentSiteID);
            }
            set
            {
                _googleSiteKey = value;
            }
        }

        public static string GoogleSecretKey
        {
            get
            {
                return SettingsKeyInfoProvider.GetValue("reCaptchaV3SecretKey", SiteContext.CurrentSiteID);
            }
            set
            {
                _googleSecretKey = value;
            }
        }

        [DefaultValueEditingComponent(TextAreaComponent.IDENTIFIER)]
        public override string DefaultValue
        {
            get;
            set;
        }
    }
}