﻿using ActivityCaptcha.FormBuilder.FormComponents;
using ActivityCaptcha.FormBuilder.FormComponents.FormComponentProperties;
using CMS.DataEngine;
using CMS.SiteProvider;
using Kentico.Forms.Web.Mvc;

// Registers a form component for use in the form builder
[assembly: RegisterFormComponent(InvisibleReCaptchaV3Component.IDENTIFIER, typeof(InvisibleReCaptchaV3Component), "Invisible ReCaptcha Component - V3", Description = "This is a custom invisible ReCaptcha v3 component", IconClass = "icon-recaptcha")]

namespace ActivityCaptcha.FormBuilder.FormComponents
{
    public class InvisibleReCaptchaV3Component : FormComponent<InvisibleReCaptchaV3Properties, string>
    {
        public const string IDENTIFIER = "InvisibleReCaptchaV3Component";

        public string GoogleSiteKey
        {
            get
            {
                var defaultValue = SettingsKeyInfoProvider.GetValue("reCaptchaV3SiteKey", SiteContext.CurrentSiteID);
                return defaultValue;
            }
            set
            {
                GoogleSiteKey = value;
            }
        }

        public string GoogleSecretKey
        {
            get
            {
                return SettingsKeyInfoProvider.GetValue("reCaptchav3SecretKey", SiteContext.CurrentSiteID);
            }
            set
            {
                GoogleSecretKey = value;
            }
        }

        // Specifies the property is used for data binding by the form builder
        [BindableProperty]
        // Used to store the value of the input field of the component
        public string Value { get; set; }

        // Gets the value of the form field instance passed from a view where the instance is rendered
        public override string GetValue()
        {
            return Value;
        }

        // Sets the default value of the form field instance
        public override void SetValue(string value)
        {
            Value = value;
        }
    }
}