﻿using System;
using System.Net;
using System.Net.Http;
using CMS.EventLog;
using ActivityCaptcha.FormBuilder.FormComponents.FormComponentProperties;
using ActivityCaptcha.Web.FormBuilder.ValidationRules;
using Kentico.Forms.Web.Mvc;
using Newtonsoft.Json.Linq;

[assembly: RegisterFormValidationRule("ReCaptchaV3Validation", typeof(ReCaptchaV3Validation), "ReCaptcha V3 Validation", Description = "Validate reCaptcha V3 token.")]

namespace ActivityCaptcha.Web.FormBuilder.ValidationRules
{
    [Serializable]
    public class ReCaptchaV3Validation : ValidationRule<string>
    {
        public override string GetTitle()
        {
            return "ReCaptcha V3 Validation";
        }

        class Data
        {
            public string Secret { get; set; }
            public string Response { get; set; }
        };

        protected override bool Validate(string value)
        {
            if (value == null || String.IsNullOrEmpty(value.ToString()))
            {
                return false;
            }

            var gRecaptchaResponse = value.ToString();

            value = "";

            return ValidateToken(InvisibleReCaptchaV3Properties.GoogleSecretKey, gRecaptchaResponse);
        }

        public static bool ValidateToken(string googleSecretKey, string gRecaptchaResponse)
        {
            HttpClient httpClient = new HttpClient();

            HttpResponseMessage res = httpClient.GetAsync($"https://www.google.com/recaptcha/api/siteverify?secret={googleSecretKey}&response={gRecaptchaResponse}").Result;

            if (res.StatusCode != HttpStatusCode.OK)
            {
                return false;
            }
            else
            {
                string JSONres = res.Content.ReadAsStringAsync().Result;

                // This is here so we can detect patterns. If too much spam is getting thru, we might need to go off of score
                EventLogProvider.LogInformation("GoogleReCaptchaV3", "Result", JSONres);

                dynamic JSONdata = JObject.Parse(JSONres);

                if (JSONdata.success != "true")
                {
                    return false;
                }

                return true;
            }
        }
    }
}